package com.seminario.sabelotodo.configuration

class Constants {

    companion object{
        private const val URL_API_BASE="/api"
        private const val URL_API_VERSION="/V1"
        private const val URL_BASE= URL_API_BASE+ URL_API_VERSION

        const val URL_BASE_PREGUNTA ="$URL_BASE/preguntas"
        const val URL_BASE_CATEGORIA ="$URL_BASE/categoria"
        const val URL_BASE_RESULTADO ="$URL_BASE/resultados"
    }
}