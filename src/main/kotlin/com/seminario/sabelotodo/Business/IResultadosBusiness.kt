package com.seminario.sabelotodo.Business
import com.seminario.sabelotodo.model.Resultado

interface IResultadoBusiness{
    fun list(): List<Resultado>
    fun load(id: Long):Resultado
    fun save(resultado: Resultado):Resultado
    fun remove(id:Long)    
}
