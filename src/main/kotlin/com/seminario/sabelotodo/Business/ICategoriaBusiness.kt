package com.seminario.sabelotodo.Business

import com.seminario.sabelotodo.model.Categoria

interface ICategoriaBusiness {

    fun list (): List<Categoria>
    fun load(idCategoria:Long) : Categoria
    fun save(categoria:Categoria) : Categoria
    fun remove(idCategoria:Long)
    
}