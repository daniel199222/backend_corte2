package com.seminario.sabelotodo.Business

import com.seminario.sabelotodo.exception.BusinessException
import com.seminario.sabelotodo.exception.NotFoundException
import com.seminario.sabelotodo.model.Categoria
import com.seminario.sabelotodo.repositorio.CategoriaRepositorio
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class CategoriaBusiness  : ICategoriaBusiness{

    @Autowired
    val categoriaRepository: CategoriaRepositorio? = null
    @Throws(BusinessException::class)
    override fun list(): List<Categoria> {
        try {
            return categoriaRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    override fun load(id_categoria: Long): Categoria{
        val op: Optional<Categoria>
        try {
            op = categoriaRepository!!.findById(id_categoria)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        if (!op.isPresent) {
            throw NotFoundException("No se encontro la pregunta con id $id_categoria")
        }
        return op.get()
    }

    override fun save(categoria: Categoria): Categoria {
        try {
            return categoriaRepository!!.save(categoria)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    override fun remove(idCategoria: Long) {
        val op: Optional<Categoria>
        try {
            op = categoriaRepository!!.findById(idCategoria)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        if (!op.isPresent) {
            throw NotFoundException("No se encontro la pregunta con id $idCategoria")
        } else {
            try {
                categoriaRepository!!.deleteById(idCategoria)
            } catch (e: Exception) {
                throw BusinessException(e.message)
            }
        }
    }


}
