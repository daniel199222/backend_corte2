package com.seminario.sabelotodo.Business
import com.seminario.sabelotodo.Business.IResultadoBusiness
import com.seminario.sabelotodo.model.Resultado
import org.springframework.beans.factory.annotation.Autowired
import com.seminario.sabelotodo.exception.BusinessException
import com.seminario.sabelotodo.exception.NotFoundException
import com.seminario.sabelotodo.repositorio.ResultadoRepositorio
import org.springframework.stereotype.Service
import java.util.*

@Service
class ResultadosBusiness: IResultadoBusiness{
    @Autowired
    val resultadoRepositorio: ResultadoRepositorio? = null

    @Throws(BusinessException::class)
    override fun list(): List<Resultado> {
        try {
            return resultadoRepositorio!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun load(id: Long): Resultado {
        val op: Optional<Resultado>
        try {
            op = resultadoRepositorio!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        if (!op.isPresent) {
            throw NotFoundException("No se encontro resultado con id $id")
        }

        return op.get()

    }

    @Throws(BusinessException::class)
    override fun save (resultado: Resultado): Resultado{
        try {
            return resultadoRepositorio!!.save(resultado)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    
    override fun remove (id: Long){
        val op: Optional<Resultado>
        try {
            op = resultadoRepositorio!!.findById(id)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        if (!op.isPresent) {
            throw NotFoundException("No se encontro respuesta con id $id")
        } else {
            try {
                resultadoRepositorio!!.deleteById(id)
            } catch (e: Exception) {
                throw BusinessException(e.message)
            }
        }
    }
}

