package com.seminario.sabelotodo.Business
import com.seminario.sabelotodo.model.Pregunta

interface IPreguntaBusiness {

    fun list(): List<Pregunta>
    fun load(idPregunta:Long): List<Pregunta>
    fun loadByCategoria(idCategoria: Long): List<Pregunta>
    fun save (pregunta:Pregunta):Pregunta
    fun remove (idPregunta:Long)
}