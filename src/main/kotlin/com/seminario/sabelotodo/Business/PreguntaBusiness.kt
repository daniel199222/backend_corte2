package com.seminario.sabelotodo.Business
import com.seminario.sabelotodo.model.Pregunta
import com.seminario.sabelotodo.repositorio.PreguntaRepositorio
import com.seminario.sabelotodo.exception.BusinessException
import com.seminario.sabelotodo.exception.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class PreguntaBusiness : IPreguntaBusiness {

    @Autowired
    val personaRepository: PreguntaRepositorio? = null

    @Throws(BusinessException::class)
    override fun list(): List<Pregunta> {
        try {
            return personaRepository!!.findAll()
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun load(idCategoria: Long): List<Pregunta> {
        val op: List<Pregunta>
        try {
            op = personaRepository!!.findByIdCategoria(idCategoria)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        return op
        /*
        val op: Optional<Pregunta>
        try {
            op = personaRepository!!.findById(idPregunta)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
        if (!op.isPresent) {
            throw NotFoundException("No se encontro la pregunta con id $idPregunta")
        }

        return op.get()*/

    }

    @Throws(BusinessException::class, NotFoundException::class)
    override fun loadByCategoria(idCategoria: Long): List<Pregunta> {
        val op: List<Pregunta>
        try {
            op = personaRepository!!.findByIdCategoria(idCategoria)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        return op
    }

    @Throws(BusinessException::class)
    override fun save(pregunta: Pregunta): Pregunta {
        try {
            return personaRepository!!.save(pregunta)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }
    }

    override fun remove(idPregunta: Long) {
        val op: Optional<Pregunta>
        try {
            op = personaRepository!!.findById(idPregunta)
        } catch (e: Exception) {
            throw BusinessException(e.message)
        }

        if (!op.isPresent) {
            throw NotFoundException("No se encontro la pregunta con id $idPregunta")
        } else {
            try {
                personaRepository!!.deleteById(idPregunta)
            } catch (e: Exception) {
                throw BusinessException(e.message)
            }
        }
    }
}