package com.seminario.sabelotodo.controller
import com.seminario.sabelotodo.Business.IResultadoBusiness
import com.seminario.sabelotodo.configuration.Constants
import com.seminario.sabelotodo.exception.BusinessException
import com.seminario.sabelotodo.exception.NotFoundException
import com.seminario.sabelotodo.model.Resultado
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.*
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constants.URL_BASE_RESULTADO)
class ResultadoRestController {

    @Autowired
    val resultadosBusiness: IResultadoBusiness?=null

    @GetMapping("")
    fun list ():ResponseEntity<List<Resultado>>{
        return try{
            ResponseEntity(resultadosBusiness!!.list(),HttpStatus.OK)
        }catch(e:Exception){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/{id}")
    fun load (@PathVariable("id") id:Long):ResponseEntity<Any>{
        return try{
            ResponseEntity(resultadosBusiness!!.load(id),HttpStatus.OK)
        }catch(e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch(e: NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping("")
    fun insert(@RequestBody resultado:Resultado):ResponseEntity<Any>{
        return try{
            resultadosBusiness!!.save(resultado)
            val responseHeader=HttpHeaders()            
            ResponseEntity(responseHeader,HttpStatus.CREATED)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("")
    fun update(@RequestBody resultado:Resultado):ResponseEntity<Any>{
        return try{
            resultadosBusiness!!.save(resultado)
            ResponseEntity(HttpStatus.OK)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @DeleteMapping("/{id}")
    fun delete (@PathVariable("id") id:Long):ResponseEntity<Any>{
        return try{
            resultadosBusiness!!.remove(id)
            ResponseEntity(HttpStatus.OK)
        }catch(e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch(e: NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    

}