package com.seminario.sabelotodo.controller

import com.seminario.sabelotodo.model.Usuario
import com.seminario.sabelotodo.repositorio.UsuarioRepositorio
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/usuario")
@Api(value = "usuario", description = "Rest API para consultar la informacion del usuario", tags = arrayOf("Usuario API"))
class UsuarioController(var usuarioRepositorio: UsuarioRepositorio) {

    @ApiOperation("Consulta la informacion del usuario")
    @ApiResponses(
            value = *arrayOf(
                    ApiResponse(code = 200, message = "OK"),
                    ApiResponse(code = 401, message = "No esta autorizado para acceder a este recurso"),
                    ApiResponse(code = 404, message = "El recurso no fue encontrado")
            )
    )
    @GetMapping
    fun getUser(@RequestHeader("username") username: String): Usuario {
        return usuarioRepositorio.findByUsername(username) ?: throw UsernameNotFoundException(username)
    }
}