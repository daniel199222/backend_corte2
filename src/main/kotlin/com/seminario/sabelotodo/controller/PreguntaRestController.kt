package com.seminario.sabelotodo.controller


import com.seminario.sabelotodo.Business.IPreguntaBusiness
import com.seminario.sabelotodo.configuration.Constants
import com.seminario.sabelotodo.exception.BusinessException
import com.seminario.sabelotodo.exception.NotFoundException
import com.seminario.sabelotodo.model.Pregunta
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.*
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constants.URL_BASE_PREGUNTA)
class PreguntaRestController {

    @Autowired
    val personaBusiness: IPreguntaBusiness?=null

    @GetMapping("")
    fun list ():ResponseEntity<List<Pregunta>>{
        return try{
            ResponseEntity(personaBusiness!!.list(),HttpStatus.OK)
        }catch(e:Exception){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/{id}")
    fun load (@PathVariable("id") idCategoria:Long):ResponseEntity<Any>{
        return try{
            ResponseEntity(personaBusiness!!.loadByCategoria(idCategoria),HttpStatus.OK)
        }catch(e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch(e: NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping("")
    fun insert(@RequestBody pregunta:Pregunta):ResponseEntity<Any>{
        return try{
            personaBusiness!!.save(pregunta)
            val responseHeader=HttpHeaders()
            responseHeader.set("location",Constants.URL_BASE_PREGUNTA+"/"+pregunta.id)
            ResponseEntity(responseHeader,HttpStatus.CREATED)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("")
    fun update(@RequestBody pregunta:Pregunta):ResponseEntity<Any>{
        return try{
            personaBusiness!!.save(pregunta)
            ResponseEntity(HttpStatus.OK)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @DeleteMapping("/{id}")
    fun delete (@PathVariable("id") idPregunta:Long):ResponseEntity<Any>{
        return try{
            personaBusiness!!.remove(idPregunta)
            ResponseEntity(HttpStatus.OK)
        }catch(e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch(e: NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

}