package com.seminario.sabelotodo.controller


import com.seminario.sabelotodo.Business.ICategoriaBusiness
import com.seminario.sabelotodo.exception.BusinessException
import com.seminario.sabelotodo.exception.NotFoundException
import com.seminario.sabelotodo.configuration.Constants
import com.seminario.sabelotodo.model.Categoria
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.*
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Constants.URL_BASE_CATEGORIA)
class CategoriaRestController {

    @Autowired
    val categoriaBusiness: ICategoriaBusiness?=null

    @GetMapping("")
    fun list ():ResponseEntity<List<Categoria>>{
        return try{
            ResponseEntity(categoriaBusiness!!.list(),HttpStatus.OK)
        }catch(e:Exception){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/{id}")
    fun load (@PathVariable("id") idCategoria:Long):ResponseEntity<Any>{
        return try{
            ResponseEntity(categoriaBusiness!!.load(idCategoria),HttpStatus.OK)
        }catch(e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch(e: NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping("")
    fun insert(@RequestBody categoria:Categoria):ResponseEntity<Any>{
        return try{
            categoriaBusiness!!.save(categoria)
            val responseHeader=HttpHeaders()
            responseHeader.set("location",Constants.URL_BASE_CATEGORIA+"/"+categoria.id)
            ResponseEntity(responseHeader,HttpStatus.CREATED)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("")
    fun update(@RequestBody categoria:Categoria):ResponseEntity<Any>{
        return try{
            categoriaBusiness!!.save(categoria)
            ResponseEntity(HttpStatus.OK)
        }catch (e:BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @DeleteMapping("/{id}")
    fun delete (@PathVariable("id") idCategoria:Long):ResponseEntity<Any>{
        return try{
            categoriaBusiness!!.remove(idCategoria)
            ResponseEntity(HttpStatus.OK)
        }catch(e: BusinessException){
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }catch(e: NotFoundException){
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

}