package com.seminario.sabelotodo.controller

import com.seminario.sabelotodo.model.Usuario
import com.seminario.sabelotodo.repositorio.UsuarioRepositorio
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/sign-up")
@Api(value = "admin", description = "Rest API para registrar un nuevo usuario", tags = arrayOf("Sign Up API"))
class SignUpController(val usuarioRepositorio: UsuarioRepositorio, val bCryptPasswordEncoder: BCryptPasswordEncoder) {

    @ApiOperation(value = "Crea un nuevo usuario")
    @ApiResponses(
            value = *arrayOf(
                    ApiResponse(code = 200, message = "OK"),
                    ApiResponse(code = 401, message = "No esta autorizado para acceder a este recurso"),
                    ApiResponse(code = 404, message = "El recurso no fue encontrado")
            )
    )
    @PostMapping
    fun signUp(@RequestBody usuario: Usuario) {
        usuario.password = bCryptPasswordEncoder.encode(usuario.password);
        usuarioRepositorio.save(usuario);
    }
}