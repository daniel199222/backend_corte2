package com.seminario.sabelotodo.servicios

import com.seminario.sabelotodo.model.Usuario
import com.seminario.sabelotodo.repositorio.UsuarioRepositorio
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class UserDetailsServiceImpl(var usuarioRepositorio: UsuarioRepositorio): UserDetailsService  {

    @Transactional(readOnly = true)
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = usuarioRepositorio.findByUsername(username) ?: throw UsernameNotFoundException(username)
        return User(user.username, user.password, emptyList())
    }

    fun save(usuario: Usuario) {
        usuarioRepositorio.save(usuario)
    }
}