package com.seminario.sabelotodo.dto

data class Greeting (val id: Long, val content: String)