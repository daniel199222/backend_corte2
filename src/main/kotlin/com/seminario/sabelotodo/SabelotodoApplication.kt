package com.seminario.sabelotodo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SabelotodoApplication

fun main(args: Array<String>) {
	SpringApplication.run(SabelotodoApplication::class.java, *args)
//	runApplication<SabelotodoApplication>(*args)
}
