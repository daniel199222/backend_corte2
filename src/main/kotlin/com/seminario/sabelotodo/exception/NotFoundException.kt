package com.seminario.sabelotodo.exception

class NotFoundException(message:String?):Exception(message)