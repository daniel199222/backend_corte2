package com.seminario.sabelotodo.repositorio

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import com.seminario.sabelotodo.model.Categoria

@Repository
interface CategoriaRepositorio : JpaRepository <Categoria,Long>
