package com.seminario.sabelotodo.repositorio

import com.seminario.sabelotodo.model.Usuario
import org.springframework.data.repository.CrudRepository

interface UsuarioRepositorio: CrudRepository<Usuario, Long> {

    fun findByUsername(username: String): Usuario?
}