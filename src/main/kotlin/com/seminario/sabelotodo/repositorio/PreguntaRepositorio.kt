package com.seminario.sabelotodo.repositorio

import com.seminario.sabelotodo.model.Pregunta
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface PreguntaRepositorio: JpaRepository<Pregunta,Long> {

    @Query("FROM Pregunta WHERE id_categoria = ?1")
    fun findByIdCategoria(idCategoria: Long): List<Pregunta>
}