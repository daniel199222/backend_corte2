package com.seminario.sabelotodo.repositorio

import com.seminario.sabelotodo.model.Resultado
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ResultadoRepositorio: JpaRepository<Resultado,Long>