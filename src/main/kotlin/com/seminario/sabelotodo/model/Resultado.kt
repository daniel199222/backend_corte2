package com.seminario.sabelotodo.model

import io.swagger.annotations.ApiModelProperty
import javax.persistence.*

@Entity
@Table(name = "resultados")
class Resultado {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0

    @Column(name = "idPregunta", nullable = false)
    @ApiModelProperty(notes = "Codigo de la pregunta", required = true)
    var idPregunta: Long = 0;

    @Column(name = "idRespuesta", nullable = false)
    @ApiModelProperty(notes = "Codigo de la respuesta", required = true)
    var idRespuesta: Long = 0;

    @Column(name = "idUsuario", nullable = false)
    @ApiModelProperty(notes = "Codigo del usuario", required = true)
    var idUsuario: Long = 0;
}