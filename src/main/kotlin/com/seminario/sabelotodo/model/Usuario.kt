package com.seminario.sabelotodo.model

import io.swagger.annotations.ApiModelProperty
import javax.persistence.*

@Entity
@Table(name = "usuario")
class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0;

    @Column(name = "username", length = 50, unique = true, nullable = false)
    @ApiModelProperty(notes = "Nombre de usuario", required = true)
    var username: String = "";

    @Column(name = "password", length = 500, nullable = false)
    @ApiModelProperty(notes = "Contraseña", required = true)
    var password: String = "";

    @Column(name = "nombres", length = 50, nullable = false)
    @ApiModelProperty(notes = "Nombre(s) de la persona", required = true)
    var nombres: String = "";

    @Column(name = "apellidos", length = 50, nullable = false)
    @ApiModelProperty(notes = "Apellido(s) de la persona", required = true)
    var apellidos: String = "";
}