package com.seminario.sabelotodo.model

import java.util.*
import javax.persistence.*

@Entity
@Table(name="preguntas")
data class Pregunta (val descripcion: String = "",val respuesta1: String = "",val respuesta2: String = "",val respuesta3: String = "",val respuesta4: String = "",val respuesta_correcta: String = "",val id_categoria: Long = 0){
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id:Long = 0

    /*
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id:Long = 0;

    @Column(name = "descripcion", length = 250, nullable = false)
    @ApiModelProperty(notes = "Texto de la pregunta", required = true)
    var descripcion: String = "";
    
    @Column(name = "respuesta1", length = 50, nullable = false)
    @ApiModelProperty(notes = "respuesta numero 1", required = true)
    var respuesta1: String = "";
    
    @Column(name = "respuesta2", length = 50, nullable = false)
    @ApiModelProperty(notes = "respuesta numero 2", required = true)
    var respuesta2: String = "";
    
    @Column(name = "respuesta3", length = 50, nullable = false)
    @ApiModelProperty(notes = "respuesta numero 3", required = true)
    var respuesta3: String = "";
    
    @Column(name = "respuesta4", length = 50, nullable = false)
    @ApiModelProperty(notes = "respuesta numero 4", required = true)
    var respuesta4: String = "";
    
    @Column(name = "respuesta_correcta", length = 50, nullable = false)
    @ApiModelProperty(notes = "respueta correcta", required = true)
    var respuesta_correcta: String = "";

    @Column(name = "idCategoria", nullable = true)
    @ApiModelProperty(notes = "Codigo de la categoria a la cual pertenece la pregunta", required = false)
    var idCategoria: Long = 0;
    */
}