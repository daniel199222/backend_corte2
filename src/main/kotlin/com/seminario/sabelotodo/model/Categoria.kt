package com.seminario.sabelotodo.model

import io.swagger.annotations.ApiModelProperty
import javax.persistence.*

@Entity
@Table(name="categorias")
class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(notes = "Codigo de la categoria", required = true)
    var id:Long = 0;

    @Column(name = "descripcion", length = 250, nullable = false)
    @ApiModelProperty(notes = "Nombre de la categoria", required = true)
    var descripcion:String = "";
}